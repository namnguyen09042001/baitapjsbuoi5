function tinhDiem() {
  var diemMon1 = document.getElementById("txt-mon1").value * 1;
  var diemMon2 = document.getElementById("txt-mon2").value * 1;
  var diemMon3 = document.getElementById("txt-mon3").value * 1;
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVuc = document.getElementById("txt-uu-tien").value * 1;
  var doiTuong = document.getElementById("txt-doi-tuong").value * 1;
  var diemUuTien = khuVuc + doiTuong;
  var contentHTML = "";
  var diemTongKet = diemMon1 + diemMon2 + diemMon3 + diemUuTien;

  if (diemMon1 < 0 || diemMon2 < 0 || diemMon3 < 0) {
    alert("Không được nhập âm nhé !!!");
  } else if (10 < diemMon1 || 10 < diemMon2 || 10 < diemMon3) {
    alert("Không được nhập trên 10đ nhé !!!");
  } else if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    var content = ` Chúc mừng bạn đã bị liệt. Rớt luôn nhé !!! </p>  `;
    contentHTML = contentHTML + content;
    // document.getElementById("result").innerHTML = contentHTML;
  } else if (diemTongKet >= diemChuan) {
    var content = ` Chúc mừng bạn đã đạt được ${diemTongKet}đ . Đậu nhé !!! </p>  `;
    contentHTML = contentHTML + content;
    // document.getElementById("result").innerHTML = contentHTML;
  } else {
    var content = ` Chúc mừng bạn đã đạt được ${diemTongKet}đ . Rớt nhé !!! </p>  `;
    contentHTML = contentHTML + content;
    // document.getElementById("result").innerHTML = contentHTML;
  }
  document.getElementById("result").innerHTML = contentHTML;
}

// BaiTap2

function tinhTienDien() {
  const kw1 = 500,
    kw2 = 650,
    kw3 = 850,
    kw4 = 1100,
    kw5 = 1300;
  var ten = document.getElementById("txt-ten").value;
  var soKW = document.getElementById("txt-kw").value * 1;
  var tienTra = 0;
  if (soKW <= 50) {
    tienTra = soKW * kw1;
  } else if (50 < soKW && soKW <= 100) {
    tienTra = 50 * kw1 + (soKW - 50) * kw2;
  } else if (100 < soKW && soKW <= 200) {
    tienTra = 50 * kw1 + 50 * kw2 + (soKW - 100) * kw3;
  } else if (200 < soKW && soKW <= 350) {
    tienTra = 50 * kw1 + 50 * kw2 + 100 * kw3 + (soKW - 200) * kw4;
  } else {
    tienTra = 50 * kw1 + 50 * kw2 + 100 * kw3 + 150 * kw4 + (soKW - 350) * kw5;
  }
  document.getElementById(
    "result2"
  ).innerHTML = `${ten} Số tiền bạn cần thanh toán là: ${tienTra}`;
}
